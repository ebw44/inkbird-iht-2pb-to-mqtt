#include "inkbird_IHT-2PB.h"

void IRAM_ATTR cron() {

    for (auto&& sched : timers) {
      sched.check();
    }
}

void wifiEvent(WiFiEvent_t event){
    lastWifiEvent = event;
    breakDelay = true;
    ESP_LOGW("WIFI", "event: %d", event);

    switch(event){
        case 5:
            net.stop();
            WiFi.reconnect();
            haveWifi = false;
            breakDelay = true;     
        break;
        case 7:
            haveWifi = true;
            breakDelay = true;
        break;
        default:
        break;
    }
}

class MyAdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks {

    void onResult(BLEAdvertisedDevice advertisedDevice) {

        if(targetAddr->equals(advertisedDevice.getAddress())){

        // there is some smart scale using the same service UUID over here... :')
        //if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(serviceUUID)) {

            pServerAddress = new BLEAddress(advertisedDevice.getAddress());

            pBLEScan->stop();
            foundInkbird = true;
            breakDelay = true;

        }
    }
};


void notifyCallback( BLERemoteCharacteristic *pBLERemoteCharacteristic, uint8_t *pData, size_t length,bool isNotify ){
    

    if(pData[2] != 2 && pData[2] != 4 && pData[2] != 6){
        //we get more stuff, probably battery and other nonsense
        return;
    }

    char buff[10];
    uint8_t probeNr = pData[2] / 2;

    // internal probe on #2 disables when external #4/#6 connected
    probes[probeNr].data(pData);

    dtostrf(probes[probeNr].latestValue(), 1, 2, buff);
    ESP_LOGW("PROBE", "probe#%i: %s", probeNr, buff);

    return;   

}



// connecting to the thermometer
bool connectToBLEServer(BLEAddress pAddress) {

    ESP_LOGI("BT", "connecting");

    if(!pClient->connect(pAddress)){
        ESP_LOGD("BT", "connection failed");
        return false;
    }

    pClient->setMTU(200);

    BLERemoteService *pRemoteService = pClient->getService(serviceUUID);

    if (pRemoteService == nullptr) {

        ESP_LOGD("BT", "retrieve service failed");
        return false;
    }

    pCharacteristic2 = pRemoteService->getCharacteristic(characteristic2);

    if (pCharacteristic2 == nullptr) {

        ESP_LOGD("BT", "retrieve pCharacteristic2 failed");
        return false;
    }

    pCharacteristic2->writeValue((uint8_t *)value2, sizeof(value2), true);


    pCharacteristic1 = pRemoteService->getCharacteristic(characteristic1);

    if (pCharacteristic1 == nullptr) {

        ESP_LOGD("BT", "retrieve pCharacteristic1 failed");
        return false;
    }

    pCharacteristic1->writeValue((uint8_t *)value1, sizeof(value1), true);

    pCharacteristic1->registerForNotify(notifyCallback);


    return true;
    
}


void checkInkbird(){

    if(pClient->isConnected()){
        foundInkbird = false;        
        return;
    }

    if(foundInkbird  && !pClient->isConnected()) {

        ESP_LOGI("BT", "found target");

        foundInkbird = connectToBLEServer(*pServerAddress);
        pBLEScan->clearResults();

        ESP_LOGI("BT", "target connected: %i", foundInkbird);

        return;

    }
    
}

void reportProbes(){


    StaticJsonDocument<100> json;
    char jsonChar[100];
    bool haveMessage = false;

    for (auto&& sensor : probes) {    

        if(!sensor.isActive(10000)){
            continue;
        }
        haveMessage = true;

        json["probes"][sensor.id] = sensor.latestValue();

    }

    if(!haveMessage){
        return;
    }

    ESP_LOGI("PROBE", "sending mqtt message");

    serializeJson(json, jsonChar);
    mqtt.publish(reportTopic, jsonChar, false, 0);
}

void setup(){

    Serial.begin(115200);

    delay(5000);


    pDevice->init("ESP32S3");

    // chip feels hot to the touch, limit TX power a bit
    pDevice->setPower(ESP_PWR_LVL_N12, ESP_BLE_PWR_TYPE_DEFAULT);
    pDevice->setPower(ESP_PWR_LVL_P3, ESP_BLE_PWR_TYPE_SCAN);

    pClient = pDevice->createClient();

    pBLEScan = pDevice->getScan();
    pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());

    pBLEScan->setActiveScan(true);
    pBLEScan->setInterval(200);
    pBLEScan->setWindow(100);

    targetAddr = new BLEAddress(config.inkbirdAddr);


    //mqtt reporting
    timers[0] = {true, config.reportInterval};
    //blescan
    timers[1] = {true, config.btScanInterval};


    probes[0] = {false, }; //reserved
    probes[1] = {true, "p1"};
    probes[2] = {true, "p2"};
    probes[3] = {true, "p3"};


    cronTimer = timerBegin(1, 80, true);
    timerAttachInterrupt(cronTimer, &cron, true);
    timerAlarmWrite(cronTimer, 500 * 1000, true);
    timerAlarmEnable(cronTimer);


    WiFi.disconnect();
    delay(1);

    WiFi.mode(WIFI_STA);
    WiFi.onEvent(wifiEvent);
    WiFi.setAutoConnect(true);
    WiFi.setAutoReconnect(true);

    WiFi.begin(config.WiFiSSID, config.WiFiPasswd);
    

    mqtt.begin(config.mqtt, net);
    mqtt.setKeepAlive(10);

    sprintf(lastWill, config.lwt, config.hostName);
    sprintf(reportTopic, config.reportTopic, config.hostName);

    mqtt.setWill(lastWill, "offline", true, 0);

    ESP_LOGW("LOG", "setup done");

}


void loop() {

    // blocking, intentionally
    checkConnections();

    checkInkbird();

    if(timers[0].run()){
        reportProbes();

    }

    if(timers[1].run()){
            
        if(!pClient->isConnected()){

            // async
            pBLEScan->start(8, nullptr, true);
            ESP_LOGD("BT", "scan started");

        }
    }
    

    loopDelay(250);

}




void loopDelay(uint16_t ms){

    static const uint8_t delayms = 20;
    uint8_t remainder = ms % delayms;
    delay(remainder);

    breakDelay = false;    
    unsigned long startedAt = millis();

    while(!breakDelay && millis() - startedAt <= ms){

        if(haveWifi){
            //ArduinoOTA.handle();
            mqtt.loop();  
        }

        delay(delayms);
    }

}

bool checkConnections(){

    uint8_t status = WiFi.status();

    /*
    it seems we can' t count on wifi.status() to return correct value
    in (rare?) cases underlying bit doesn't get set and status keeps returning 3

    the event on change does get called, so we'll just lean on getting an IP

    */

    /*
    if(status == 3 && lastWifiEvent == 5){
        // fishy
        ESP_LOGE("WiFi", "fishy stuff, connected and not: %d", lastWifiEvent);
    }
    */

    if(haveWifi && mqtt.connected()){
        return 1;
    }
    
    /*
    if(status == 3 && mqtt.connected() && lastWifiEvent != 5){
        haveWifi = true;
        return 1;
    }
    */


    uint8_t retryCounter = 0;
    uint16_t delayFor;

    if(!haveWifi){

        ESP_LOGI("WiFi", "Wait for (re)connection");
        
        retryCounter = 0;        
    
        while(!haveWifi){


            if(retryCounter == 40 || status == 4){
                // automatic reconnection probably failed, restart it
                ESP_LOGE("WiFi", "attempting new connection");

                // freshen up
                WiFi.reconnect();
                delay(1);

            }

            if(retryCounter >= 92){
                // we waited for 3 minutes now, time to try the 'ol everything off and on again
                ESP_LOGE("WiFi", "unable to connect");
                delay(500);
                ESP.restart();
                return 0;

            }

            retryCounter++;
            delayFor = 100 + (floor(retryCounter / 10) * 450);

            // this might not actually delay
            status = WiFi.waitForConnectResult(delayFor);

            ESP_LOGI("WiFi", "haveWifi loop: %i", millis());


        }

        ESP_LOGI("WiFi", "connected");

    }

    if (!mqtt.connected()){
        ESP_LOGI("MQTT", "connecting");


        retryCounter = 0;

        while (!mqtt.connect(config.hostName)) {

            if(retryCounter >= 150){
                // 3 seconds have passed, release
                WiFi.reconnect();
                ESP_LOGE("MQTT", "Unable to connect");
                return 0;
            }

            retryCounter++;
            delay(20);
        }

        ESP_LOGI("MQTT", "connected");

        mqtt.publish(lastWill, "online", false, 0);

    }

    return 1;

}



